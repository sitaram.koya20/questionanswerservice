package com.stackoverflow.QuestonAnswerService.repository;

import com.stackoverflow.QuestonAnswerService.model.Badge;
import com.stackoverflow.QuestonAnswerService.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUserName(String userName);

//    List<User> findUsersByBadges(Badge badge);

}
