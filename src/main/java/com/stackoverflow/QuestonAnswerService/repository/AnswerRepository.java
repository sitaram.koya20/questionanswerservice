package com.stackoverflow.QuestonAnswerService.repository;

import com.stackoverflow.QuestonAnswerService.model.Answer;

import com.stackoverflow.QuestonAnswerService.model.Comment;
import com.stackoverflow.QuestonAnswerService.model.Question;
import com.stackoverflow.QuestonAnswerService.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface AnswerRepository extends JpaRepository<Answer, Long> {

    List<Answer> findByQuestion(Question question);

    List<Answer> findAllByUser(User user);

    Answer findAnswerByUser(User user);


}
