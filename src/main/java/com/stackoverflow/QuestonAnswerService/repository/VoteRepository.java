package com.stackoverflow.QuestonAnswerService.repository;

import com.stackoverflow.QuestonAnswerService.model.*;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VoteRepository extends JpaRepository<Vote, Long> {

    Optional<Vote> findTopByAnswerAndUserOrderByVoteIdDesc(Answer answer, User user);

    Vote getVoteByVoteType(VoteType voteType);
}
