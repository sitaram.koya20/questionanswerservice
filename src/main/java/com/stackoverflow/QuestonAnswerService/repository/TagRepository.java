package com.stackoverflow.QuestonAnswerService.repository;

import com.stackoverflow.QuestonAnswerService.model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TagRepository extends JpaRepository<Tag, Long> {

    Tag findByTagName(String tagName);

}
