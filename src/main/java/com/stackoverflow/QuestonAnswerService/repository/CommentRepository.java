package com.stackoverflow.QuestonAnswerService.repository;

import com.stackoverflow.QuestonAnswerService.model.Answer;
import com.stackoverflow.QuestonAnswerService.model.Comment;
import com.stackoverflow.QuestonAnswerService.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {

    List<Comment> findCommentByAnswer(Answer answer);

    List<Comment> findCommentByUser(User user);
}
