package com.stackoverflow.QuestonAnswerService.repository;

import com.stackoverflow.QuestonAnswerService.model.Answer;
import com.stackoverflow.QuestonAnswerService.model.Badge;
import com.stackoverflow.QuestonAnswerService.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BadgeRepository extends JpaRepository<Badge, Long> {

    Badge findByBadgeName(String badgeName);
}
