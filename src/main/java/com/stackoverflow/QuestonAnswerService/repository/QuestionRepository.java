package com.stackoverflow.QuestonAnswerService.repository;

import com.stackoverflow.QuestonAnswerService.model.Question;

import com.stackoverflow.QuestonAnswerService.model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface QuestionRepository extends JpaRepository<Question, Long> {

    List<Question> findQuestionByTags(Tag tags);
//
//    Question findByUserId(Long userId);

}
