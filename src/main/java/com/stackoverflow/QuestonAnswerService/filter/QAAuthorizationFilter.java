package com.stackoverflow.QuestonAnswerService.filter;

import com.stackoverflow.QuestonAnswerService.client.AuthorizationService;
import com.stackoverflow.QuestonAnswerService.model.RequestType;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Slf4j
public class QAAuthorizationFilter extends OncePerRequestFilter {

    @Autowired
    private AuthorizationService authorizationService;


    @SneakyThrows
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if(authorizationService == null){
            ServletContext servletContext = request.getServletContext();
            WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
            authorizationService = webApplicationContext.getBean(AuthorizationService.class);
        }
        if (!request.getServletPath().startsWith("/stackoverflow/users")
                || request.getServletPath().startsWith("\"/v2/api-docs\",\n" +
                                        "\"/configuration/ui\",\n" +
                                        "\"/swagger-resources/**\",\n" +
                                        "\"/configuration/security\",\n" +
                                        "\"/swagger-ui.html\",\n" +
                                        "\"/webjars/**\"")) {
            RequestType requestType= new RequestType();
            requestType.setToken(request.getHeader(AUTHORIZATION));
            requestType.setMethodType(request.getMethod());
            requestType.setUrlPath(request.getServletPath());
            RequestType processedRT = authorizationService.authorizeRequest(requestType);
            request.setAttribute("user",processedRT.getLoggedInUserName());
            log.info("The logged in userName is " + request.getAttribute("user") );
        }
        filterChain.doFilter(request,response);
    }
}
