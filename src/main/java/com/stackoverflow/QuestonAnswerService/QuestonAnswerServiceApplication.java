package com.stackoverflow.QuestonAnswerService;

import com.stackoverflow.QuestonAnswerService.model.RequestType;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.TemplateEngine;

@SpringBootApplication
@EnableScheduling
@EnableAsync
public class QuestonAnswerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuestonAnswerServiceApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

//	@Bean
//	public TemplateEngine templateEngine(){
//		return new TemplateEngine();
//	}

//	@Bean
//	public RequestType requestType(){
//		return new RequestType();
//	}

}
