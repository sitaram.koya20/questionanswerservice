package com.stackoverflow.QuestonAnswerService.service;

import com.stackoverflow.QuestonAnswerService.Exceptions.AnswerNotFoundException;
import com.stackoverflow.QuestonAnswerService.Exceptions.StackOverFlowException;
import com.stackoverflow.QuestonAnswerService.dto.VoteDto;
import com.stackoverflow.QuestonAnswerService.model.*;
import com.stackoverflow.QuestonAnswerService.repository.AnswerRepository;
import com.stackoverflow.QuestonAnswerService.repository.UserRepository;
import com.stackoverflow.QuestonAnswerService.repository.VoteRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.jws.soap.SOAPBinding;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import java.util.Optional;

import static com.stackoverflow.QuestonAnswerService.model.VoteType.DOWNVOTE;
import static com.stackoverflow.QuestonAnswerService.model.VoteType.UPVOTE;

@Service
@Transactional
@Slf4j
@AllArgsConstructor
public class VoteService {

    private final VoteRepository voteRepository;
    private final AnswerRepository answerRepository;
    private final UserRepository userRepository;
    private final HttpServletRequest request;

    public void saveVote(VoteDto voteDto){
        Object loggedInUserName =  request.getAttribute("user");
        Answer answer = answerRepository.findById(voteDto.getAnswerId())
                .orElseThrow(() -> new AnswerNotFoundException("Answer not found with id: " + voteDto.getAnswerId()));
        User user = userRepository.findByUserName(loggedInUserName.toString());
        Optional<Vote> voteByPostAndUser = voteRepository.findTopByAnswerAndUserOrderByVoteIdDesc(answer, user);
        if(voteByPostAndUser.isPresent() &&
                voteByPostAndUser.get().getVoteType()
                .equals(voteDto.getVoteType())){
            throw new StackOverFlowException("You have already "
                    + voteDto.getVoteType() + "'d for this answer");
        }
        if (VoteType.UPVOTE.equals(voteDto.getVoteType())) {
            answer.setVoteCount(answer.getVoteCount() + 1);
        } else {
            answer.setVoteCount(answer.getVoteCount() - 1);
        }
        voteRepository.save(mapToVote(voteDto, answer));
        answerRepository.save(answer);

    }

    private Vote mapToVote(VoteDto voteDto, Answer answer) {
        Object loggedInUserName =  request.getAttribute("user");
        User user = userRepository.findByUserName(loggedInUserName.toString());
        return Vote.builder()
                .voteType(voteDto.getVoteType())
                .answer(answer)
                .user(user)
                .build();
    }
    public Vote getVoteById(Long voteId) { return voteRepository.getById(voteId);}

    public Vote getVoteByType(VoteType voteType) { return voteRepository.getVoteByVoteType(voteType);}

    public Vote mapVoteToAnswer(VoteType voteType, Long answerId){
        Vote vote = getVoteByType(voteType);
        Answer answer = answerRepository.getById(answerId);
        if(vote.getVoteType() == UPVOTE)
            vote.setAnswer(answer);
        else if (vote.getVoteType() == DOWNVOTE)
            vote.setAnswer(answer);

        return vote;
    }

    public Vote mapVoteToUser(VoteType voteType, Long userId){
        Vote vote = getVoteByType(voteType);
        User user = userRepository.getById(userId);
        if(vote.getVoteType() == UPVOTE)
            vote.setUser(user);
        else if (vote.getVoteType() == DOWNVOTE)
            vote.setUser(user);
        return vote;
    }

}
