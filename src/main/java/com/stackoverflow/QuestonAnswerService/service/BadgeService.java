package com.stackoverflow.QuestonAnswerService.service;


import com.stackoverflow.QuestonAnswerService.model.Answer;
import com.stackoverflow.QuestonAnswerService.model.Badge;
import com.stackoverflow.QuestonAnswerService.model.Comment;
import com.stackoverflow.QuestonAnswerService.model.User;
import com.stackoverflow.QuestonAnswerService.repository.AnswerRepository;
import com.stackoverflow.QuestonAnswerService.repository.BadgeRepository;
import com.stackoverflow.QuestonAnswerService.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@Slf4j
@AllArgsConstructor
public class BadgeService {

    private final BadgeRepository badgeRepository;
    private final UserRepository userRepository;
    private final AnswerRepository answerRepository;

    public void saveBadge(Badge badge){
        badgeRepository.save(badge);
    }

    public Badge getBadgeById(Long badgeId){
        return badgeRepository.getById(badgeId);
    }

    public String badgeUser(User user){
        Badge badge = new Badge();
        Answer answer = answerRepository.findAnswerByUser(user);

        if(answer.getVoteCount() <= 0){
            badge.setBadgeName("BRONZE");
        } else {
            badge.setBadgeName("GOLD");
        }
        badgeRepository.save(badge);
        return badge.getBadgeName();
    }

}
