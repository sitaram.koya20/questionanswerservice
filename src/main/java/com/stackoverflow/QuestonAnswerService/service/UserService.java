package com.stackoverflow.QuestonAnswerService.service;

import com.stackoverflow.QuestonAnswerService.dto.UserDto;
import com.stackoverflow.QuestonAnswerService.mapper.UserMapper;
import com.stackoverflow.QuestonAnswerService.model.Tag;
import com.stackoverflow.QuestonAnswerService.model.User;
import com.stackoverflow.QuestonAnswerService.repository.TagRepository;
import com.stackoverflow.QuestonAnswerService.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
@AllArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final TagRepository tagRepository;
    private final UserMapper userMapper;


    public User getUserById(Long id){
        return userRepository.getById(id);
    }

    public List<UserDto> getAllUsers(){

        return userRepository.findAll()
                .stream()
                .map(userMapper::mapToDto)
                .collect(toList());
    }

    public void saveUser(User user){
        userRepository.save(user);
    }

    public User getUserbyUserName(String userName){
        return userRepository.findByUserName(userName);
    }

    public User updateUserTag(String userName, String tagName) {
        User user = getUserbyUserName(userName);
        Tag tag = tagRepository.findByTagName(tagName);
        user.getTags().add(tag);
        return user;
    }
}
