package com.stackoverflow.QuestonAnswerService.service;

import com.stackoverflow.QuestonAnswerService.Exceptions.QuestionNotFoundException;
import com.stackoverflow.QuestonAnswerService.Exceptions.StackOverFlowException;
import com.stackoverflow.QuestonAnswerService.Exceptions.TagNotFoundException;
import com.stackoverflow.QuestonAnswerService.dto.QuestionDto;
import com.stackoverflow.QuestonAnswerService.mapper.QuestionMapper;
import com.stackoverflow.QuestonAnswerService.model.Question;
import com.stackoverflow.QuestonAnswerService.model.Tag;
import com.stackoverflow.QuestonAnswerService.model.User;
import com.stackoverflow.QuestonAnswerService.repository.QuestionRepository;
import com.stackoverflow.QuestonAnswerService.repository.TagRepository;
import com.stackoverflow.QuestonAnswerService.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
@Slf4j
@AllArgsConstructor
public class QuestionService {

    private final QuestionRepository questionRepository;
    private final TagRepository tagRepository;
    private final UserRepository userRepository;
    private final QuestionMapper questionMapper;

    public void saveQuestion(Question question){

       boolean checkQuestionExists = questionRepository.findAll().contains(question.getQuestionId());
       if(!checkQuestionExists)
        questionRepository.save(question);
       else
           throw new StackOverFlowException("The Question with title " + question.getQuestionTitle() + "exists");
    }

    public List<QuestionDto> getAllQuestions(){
        return questionRepository.findAll()
                .stream()
                .map(questionMapper::mapToDto)
                .collect(toList());
    }

    public Question getQuestionById(Long questionId){
        return questionRepository.getById(questionId);
    }

    public Question mapQuestionToTag(String tagName, Long questionId){
        Question question = getQuestionById(questionId);
        Tag tag = tagRepository.findByTagName(tagName);
//        List<Question> test = questionRepository.findQuestionByTags(tag);
//        System.out.println(test);
        question.getTags().add(tag);
        return question;
    }

    public Question mapQuestionToUser(Long userId, Long questionId) {
        Question question = getQuestionById(questionId);
        User user = userRepository.getById(userId);
        question.setUser(user);
        return question;

    }
}
