package com.stackoverflow.QuestonAnswerService.service;
import com.stackoverflow.QuestonAnswerService.Exceptions.QuestionNotFoundException;
import com.stackoverflow.QuestonAnswerService.Exceptions.StackOverFlowException;
import com.stackoverflow.QuestonAnswerService.client.AuthorizationService;
import com.stackoverflow.QuestonAnswerService.dto.AnswerRequest;
import com.stackoverflow.QuestonAnswerService.dto.AnswerResponse;
import com.stackoverflow.QuestonAnswerService.mapper.AnswerMapper;
import com.stackoverflow.QuestonAnswerService.model.*;
import com.stackoverflow.QuestonAnswerService.repository.AnswerRepository;
import com.stackoverflow.QuestonAnswerService.repository.QuestionRepository;
import com.stackoverflow.QuestonAnswerService.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
@AllArgsConstructor
public class AnswerService {
    private static final String QUESTION_URL = "";
    private final AnswerRepository answerRepository;
    private final QuestionRepository questionRepository;
    private final UserRepository userRepository;
    private final AnswerMapper answerMapper;
    private final MailContentBuilder mailContentBuilder;
    private final AuthorizationService authorizationService;
    private final MailService mailService;
    private final HttpServletRequest request;

    public void saveAnswer(AnswerRequest answerRequest) throws Exception {
        Object loggedInUser =  request.getAttribute("user");

       Question question = questionRepository.findById(answerRequest.getQuestionId())
               .orElseThrow(() -> new QuestionNotFoundException(answerRequest.getQuestionId().toString()));
       User user = userRepository.findById(answerRequest.getUserId())
               .orElseThrow(()-> new StackOverFlowException("User with " +answerRequest.getUserId() + "not found"));

       answerRepository.save(answerMapper.map(answerRequest));

        String message = mailContentBuilder.build(loggedInUser.toString()
                + " posted a answer on your question." + QUESTION_URL);
        sendAnswersNotification(message, question.getUser());
    }

    private void sendAnswersNotification(String message, User user) {
        Object loggedInUser =  request.getAttribute("user");
        mailService.sendMail(new NotificationEmail( loggedInUser.toString() + " Answered on " + user.getUserName() + "'s Question",
                user.getEmail(), message));
    }

    public List<AnswerResponse> getAllAnswers() {
        return answerRepository.findAll()
                .stream()
                .map(answerMapper::mapToDto)
                .collect(toList());
    }

    public Answer getAnswerById(Long answerId){
        return answerRepository.getById(answerId);
    }

    public List<AnswerResponse> getAllAnswersForQuestion(Long questionId){
        Question question = questionRepository.findById(questionId)
                .orElseThrow(() -> new QuestionNotFoundException(questionId.toString()));
        return answerRepository.findByQuestion(question)
                .stream()
                .map(answerMapper::mapToDto).collect(toList());
    }

    public List<AnswerResponse> getAllAnswersForUser(Long userId){
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UsernameNotFoundException(userRepository.findById(userId).toString()));
        return answerRepository.findAllByUser(user)
                .stream()
                .map(answerMapper::mapToDto)
                .collect(toList());
    }

    public Answer mapAnswerToQuestion(Long questionId, Long answerId){

        Answer answer = getAnswerById(answerId);
        Question question = questionRepository.getById(questionId);
        answer.setQuestion(question);
        return answer;
    }

    public Answer mapAnswerToUserId(Long userId, Long answerId){
        Answer answer = getAnswerById(answerId);
        User user = userRepository.getById(userId);
        answer.setUser(user);
        return answer;
    }

//    public Answer getAnswerByUserId(Long userId){
//        return answerRepository.findAnswerByUserId(userId);
//    }

}
