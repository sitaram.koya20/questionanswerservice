package com.stackoverflow.QuestonAnswerService.service;

import com.stackoverflow.QuestonAnswerService.model.Tag;
import com.stackoverflow.QuestonAnswerService.repository.TagRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
@Transactional
public class TagService {

    private final TagRepository tagRepository;

    public void save(Tag tag){
        tagRepository.save(tag);
    }

    public List<Tag> getAllTags(){
        return tagRepository.findAll();
    }

    public Tag getTagById(Long tagId){

        return tagRepository.getById(tagId);
    }

    public void deleteTagById(Long tagId) {
        tagRepository.deleteById(tagId);
    }
}
