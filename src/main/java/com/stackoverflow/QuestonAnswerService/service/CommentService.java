package com.stackoverflow.QuestonAnswerService.service;

import com.stackoverflow.QuestonAnswerService.model.Answer;
import com.stackoverflow.QuestonAnswerService.model.Comment;
import com.stackoverflow.QuestonAnswerService.model.User;
import com.stackoverflow.QuestonAnswerService.repository.AnswerRepository;
import com.stackoverflow.QuestonAnswerService.repository.CommentRepository;
import com.stackoverflow.QuestonAnswerService.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
@Slf4j
@AllArgsConstructor
public class CommentService {

    private final CommentRepository commentRepository;
    private final AnswerRepository answerRepository;
    private final UserRepository userRepository;

    public void saveComment(Comment comment){

        commentRepository.save(comment);
    }

//    public List<Comment> getAllComments() {
//        return answerRepository.findAll();
//    }
//
    public Comment getCommentById(Long commentId){
        return commentRepository.getById(commentId);
    }

    public Comment mapCommentToAnswer(Long answerId, Long commentId) {
        Comment comment = getCommentById(commentId);
        Answer answer =  answerRepository.getById(answerId);
    //    List<Comment> comm = commentRepository.findCommentByAnswer(answer);
     //   System.out.println(comm);
        comment.setAnswer(answer);
        return comment;
    }

    public Comment mapCommentToUser(Long userId, Long commentId){
        Comment comment = getCommentById(commentId);
        User user = userRepository.getById(userId);
     //   List<Comment> com = commentRepository.findCommentByUser(user);
     //   System.out.println(com);
        comment.setUser(user);
        return  comment;


    }

}
