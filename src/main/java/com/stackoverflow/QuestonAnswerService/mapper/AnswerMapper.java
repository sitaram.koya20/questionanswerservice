package com.stackoverflow.QuestonAnswerService.mapper;

import com.stackoverflow.QuestonAnswerService.dto.AnswerResponse;
import com.stackoverflow.QuestonAnswerService.dto.AnswerRequest;
import com.stackoverflow.QuestonAnswerService.model.*;
import com.stackoverflow.QuestonAnswerService.repository.UserRepository;
import com.stackoverflow.QuestonAnswerService.repository.VoteRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Mapper(componentModel = "spring")
public abstract class AnswerMapper {
//    private CommentRepository commentRepository;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private VoteRepository voteRepository;

    @Autowired
    private UserRepository userRepository;

    @Mapping(target = "user.userId", source = "userId")
    @Mapping(target = "question.questionId", source = "questionId")
    @Mapping(target = "voteCount", constant = "0")
    public abstract Answer map(AnswerRequest answerRequest);


    @Mapping(target = "questionId", source = "question.questionId")
    @Mapping(target = "userName", source = "user.userName")
//    @Mapping(target = "commentCount", expression = "java(commentCount(answer))")
    public abstract AnswerResponse mapToDto(Answer answer);

//
//    Integer commentCount(Answer answer) {
//        return commentRepository.findCommentByAnswer(answer).size();
//    }

}

