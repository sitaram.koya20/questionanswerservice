package com.stackoverflow.QuestonAnswerService.mapper;

import com.stackoverflow.QuestonAnswerService.dto.QuestionDto;
import com.stackoverflow.QuestonAnswerService.model.Question;
import com.stackoverflow.QuestonAnswerService.model.Tag;
import com.stackoverflow.QuestonAnswerService.repository.AnswerRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

@Mapper(componentModel = "spring")
public abstract class QuestionMapper {
    @Autowired
    private AnswerRepository answerRepository;

    @Mapping(target = "tagCount", expression = "java(mapTag(question.getTags()))")
    @Mapping(target = "userName", source = "user.userName")
    @Mapping(target = "answerCount", expression = "java(answerCount(question))")
    public abstract QuestionDto mapToDto(Question question);

    @Mapping(target = "tags", ignore = true)
    @Mapping(target = "user.userName", source = "userName")
    public abstract Question map(QuestionDto questionDto);

    Integer mapTag(Set<Tag> tagCount){
        return tagCount.size();
    }

    Integer answerCount(Question question) {
        return answerRepository.findByQuestion(question).size();
    }

}
