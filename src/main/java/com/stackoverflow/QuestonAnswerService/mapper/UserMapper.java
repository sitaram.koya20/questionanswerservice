package com.stackoverflow.QuestonAnswerService.mapper;

import com.stackoverflow.QuestonAnswerService.dto.UserDto;
import com.stackoverflow.QuestonAnswerService.model.Answer;
import com.stackoverflow.QuestonAnswerService.model.Tag;
import com.stackoverflow.QuestonAnswerService.model.User;
import com.stackoverflow.QuestonAnswerService.repository.AnswerRepository;
import com.stackoverflow.QuestonAnswerService.service.BadgeService;
import org.mapstruct.InheritConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

@Mapper(componentModel = "spring")
public abstract class UserMapper {

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private BadgeService badgeService;

    @InheritConfiguration
    @Mapping(target = "tags", ignore = true)
//    @Mapping(target = "badgeGiven", ignore = true)
    public abstract User map(UserDto userDto);

    @Mapping(target = "tagCount", expression = "java(mapTags(user.getTags()))")
    @Mapping(target = "badgeGiven", defaultValue = "BRONZE")
    public abstract UserDto mapToDto(User user);

     Integer mapTags(Set<Tag> tagCount) {
        return tagCount.size();
    }

     String badgeUser(User user){
        return badgeService.badgeUser(user);
    }
}

