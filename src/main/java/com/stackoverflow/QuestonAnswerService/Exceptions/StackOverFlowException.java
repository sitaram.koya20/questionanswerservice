package com.stackoverflow.QuestonAnswerService.Exceptions;

public class StackOverFlowException extends RuntimeException {
    public StackOverFlowException(String exMessage, Exception exception) {
        super(exMessage, exception);
    }

    public StackOverFlowException(String exMessage) {
        super(exMessage);
    }
}
