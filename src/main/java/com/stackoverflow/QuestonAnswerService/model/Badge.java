package com.stackoverflow.QuestonAnswerService.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Badge {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long badgeId;
    private String badgeName;


    // gold (A, B, C), silver (B, D, E A, Q), bronze (A, H, L , E)
}
