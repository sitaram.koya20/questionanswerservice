package com.stackoverflow.QuestonAnswerService.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long userId;

    private String userName;
    private String email;
    private String badgeGiven = "";

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private Set<Tag> tags = new HashSet<>();

//    @OneToMany(fetch = FetchType.LAZY)
//    private Collection<Question> questions;

//    @OneToMany(fetch = FetchType.LAZY)
//    private Collection<Answer> answers;

//    @OneToMany(fetch = FetchType.LAZY)
//    private Collection<Vote> votes;

//    @ManyToMany
//    private Collection<Badge> badges;

}
