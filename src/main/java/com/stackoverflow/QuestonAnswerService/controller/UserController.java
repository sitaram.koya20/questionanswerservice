package com.stackoverflow.QuestonAnswerService.controller;

import com.stackoverflow.QuestonAnswerService.dto.UserDto;
import com.stackoverflow.QuestonAnswerService.model.User;
import com.stackoverflow.QuestonAnswerService.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.ResponseEntity.status;


import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
@RequestMapping("/stackoverflow/users")
public class UserController {

    private final UserService userService;



    @PostMapping("/saveUser")
    public ResponseEntity<?> saveUser(@RequestBody User user){
        userService.saveUser(user);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/all")
    public ResponseEntity<List<UserDto>> getAllUsers(){
        return status(HttpStatus.OK).body(userService.getAllUsers());
    }

    @GetMapping("/{userId}")
    public ResponseEntity<User> getUserById(@PathVariable Long userId){
        return status(HttpStatus.OK).body(userService.getUserById(userId));
    }

    @GetMapping("/{userName}")
    public ResponseEntity<User> getUserByUserName(@PathVariable String userName){
        return status(HttpStatus.OK).body(userService.getUserbyUserName(userName));
    }

    @PutMapping("/{userName}/{tagName}")
    public ResponseEntity<User> getUserByUserName(@PathVariable String userName, @PathVariable String tagName){
        return status(HttpStatus.OK).body(userService.updateUserTag(userName,tagName));
    }


}
