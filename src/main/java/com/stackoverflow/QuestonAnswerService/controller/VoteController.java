package com.stackoverflow.QuestonAnswerService.controller;


import com.stackoverflow.QuestonAnswerService.dto.VoteDto;
import com.stackoverflow.QuestonAnswerService.model.Question;
import com.stackoverflow.QuestonAnswerService.model.Vote;
import com.stackoverflow.QuestonAnswerService.model.VoteType;
import com.stackoverflow.QuestonAnswerService.service.VoteService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.ResponseEntity.status;

@RestController
@AllArgsConstructor
@RequestMapping("/stackoverflow/votes")
public class VoteController {

    private final VoteService voteService;

    @PostMapping("")
    public ResponseEntity<?> saveVote(@RequestBody VoteDto voteDto){
        voteService.saveVote(voteDto);
        return  new ResponseEntity<>(HttpStatus.CREATED);
    }
    @GetMapping("/{voteId}")
    public ResponseEntity<Vote> getVoteById(@PathVariable Long voteId){
        return status(HttpStatus.OK).body(voteService.getVoteById(voteId));
    }

    @PutMapping("/answer/{voteType}/{answerId}")
    public ResponseEntity<Vote> mapVoteToAnswer(@PathVariable VoteType voteType, @PathVariable Long answerId){
        return status(HttpStatus.OK).body(voteService.mapVoteToAnswer(voteType, answerId));
    }

    @PutMapping("/user/{voteType}/{userId")
    public ResponseEntity<Vote> mapVoteToUser(@PathVariable VoteType voteType, @PathVariable Long userId){
        return status(HttpStatus.OK).body(voteService.mapVoteToUser(voteType, userId));
    }



}
