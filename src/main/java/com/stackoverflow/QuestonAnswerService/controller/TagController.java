package com.stackoverflow.QuestonAnswerService.controller;

import com.stackoverflow.QuestonAnswerService.model.Tag;
import com.stackoverflow.QuestonAnswerService.service.TagService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

import static org.springframework.http.ResponseEntity.status;

@RestController
@Transactional
@AllArgsConstructor
@RequestMapping("stackoverflow/tags")
public class TagController {

    private final TagService tagService;

    @PostMapping("")
    public ResponseEntity<?> saveTag(@RequestBody Tag tag){
        tagService.save(tag);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("")
    public ResponseEntity<List<Tag>> getAllTags(){
        return status(HttpStatus.OK).body(tagService.getAllTags());
    }

    @GetMapping("/{tagId}")
    public ResponseEntity<Tag> getTagById(@PathVariable Long tagId){
        return status(HttpStatus.OK).body(tagService.getTagById(tagId));
    }

    @DeleteMapping("/{tagId}")
    public ResponseEntity<?> deleteTagById(@PathVariable Long tagId){
        tagService.deleteTagById(tagId);
        return new ResponseEntity<>(HttpStatus.OK);

    }
}
