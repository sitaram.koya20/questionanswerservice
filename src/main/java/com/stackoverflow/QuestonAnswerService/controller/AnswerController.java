package com.stackoverflow.QuestonAnswerService.controller;
import com.stackoverflow.QuestonAnswerService.dto.AnswerRequest;
import com.stackoverflow.QuestonAnswerService.dto.AnswerResponse;
import com.stackoverflow.QuestonAnswerService.model.Answer;
import com.stackoverflow.QuestonAnswerService.service.AnswerService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.ResponseEntity.status;

@RestController
@AllArgsConstructor
@RequestMapping("/stackoverflow/answers")
public class AnswerController {

    private final AnswerService answerService;

    @PostMapping("")
    public ResponseEntity<?> saveAnswer(@RequestBody AnswerRequest answerRequest) throws Exception {
        answerService.saveAnswer(answerRequest);
        return  new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/all")
    public ResponseEntity<List<AnswerResponse>> getAllAnswers(){
        return status(HttpStatus.OK).body(answerService.getAllAnswers());
    }

    @GetMapping("/{answerId}")
    public ResponseEntity<Answer> getAnswerById(@PathVariable Long answerId){
        return status(HttpStatus.OK).body(answerService.getAnswerById(answerId));
    }

    @PutMapping("/question/{questionId}/{answerId}")
    public ResponseEntity<Answer> mapAnswerToQuestion(@PathVariable Long questionId, @PathVariable Long answerId){
        return status(HttpStatus.OK).body(answerService.mapAnswerToQuestion(questionId, answerId));
    }

    @PutMapping("/user/{userId}/{answerId}")
    public ResponseEntity<Answer> mapAnswerToUser(@PathVariable Long userId, @PathVariable Long answerId){
        return status(HttpStatus.OK).body(answerService.mapAnswerToUserId(userId, answerId));
    }

//    @GetMapping("/{questionId}")
//    public ResponseEntity<Answer> getAnswerByQuestionId(@PathVariable Long questionId){
//        return status(HttpStatus.OK).body(answerService.getAnswerByQuestionId(questionId));
//    }
//
//    @GetMapping("/{userId}")
//    public ResponseEntity<Answer> getAnswerByUserId(@PathVariable Long userId){
//        return status(HttpStatus.OK).body(answerService.getAnswerByUserId(userId));
//    }
}
