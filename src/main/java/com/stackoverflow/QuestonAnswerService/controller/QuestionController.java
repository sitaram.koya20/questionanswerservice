package com.stackoverflow.QuestonAnswerService.controller;

import com.stackoverflow.QuestonAnswerService.dto.QuestionDto;
import com.stackoverflow.QuestonAnswerService.model.Answer;
import com.stackoverflow.QuestonAnswerService.model.Question;
import com.stackoverflow.QuestonAnswerService.service.QuestionService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.ResponseEntity.status;

@RestController
@AllArgsConstructor
@RequestMapping("/stackoverflow/questions")
public class QuestionController {

    private final QuestionService questionService;

    @PostMapping("")
    public ResponseEntity<?> saveQuestion(@RequestBody Question question){
        questionService.saveQuestion(question);
        return  new ResponseEntity<>(HttpStatus.CREATED);

    }

    @GetMapping("/all")
    public ResponseEntity<List<QuestionDto>> getAllQuestions(){
        return status(HttpStatus.OK).body(questionService.getAllQuestions());
    }

    @GetMapping("/{questionId}")
    public ResponseEntity<Question> getQuestionById(@PathVariable Long questionId){
        return status(HttpStatus.OK).body(questionService.getQuestionById(questionId));
    }

    @PutMapping("/tag/{tagName}/{questionId}")
    public ResponseEntity<Question> mapAnswerToQuestion(@PathVariable String tagName, @PathVariable Long questionId){
        return status(HttpStatus.OK).body(questionService.mapQuestionToTag(tagName, questionId));
    }

    @PutMapping("/user/{userId}/{questionId}")
    public ResponseEntity<Question> mapQuestionToUser(@PathVariable Long userId, @PathVariable Long questionId){
            return status(HttpStatus.OK).body(questionService.mapQuestionToUser(userId, questionId));
    }
}
