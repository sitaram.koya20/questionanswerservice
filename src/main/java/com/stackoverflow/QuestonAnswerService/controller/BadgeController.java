package com.stackoverflow.QuestonAnswerService.controller;

import com.stackoverflow.QuestonAnswerService.model.Badge;
import com.stackoverflow.QuestonAnswerService.model.Comment;
import com.stackoverflow.QuestonAnswerService.service.BadgeService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.ResponseEntity.status;

@RestController
@AllArgsConstructor
@RequestMapping("/stackoverflow/badges")
public class BadgeController {

    private final BadgeService badgeService;

    @PostMapping("")
    public ResponseEntity<?> saveBadge(@RequestBody Badge badge){
        badgeService.saveBadge(badge);
        return  new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/{badgeId}")
    public ResponseEntity<Badge> getBadgeById(@PathVariable Long badgeId){
        return status(HttpStatus.OK).body(badgeService.getBadgeById(badgeId));
    }

//    @GetMapping("/{badgeName}")
//    public ResponseEntity<Badge> getBadgeByName(@PathVariable String badgeName){
//        return status(HttpStatus.OK).body(badgeService.getBadgeByName(badgeName));
//    }

//    @PutMapping("/user/{userId}/{badgeId}")
//    public ResponseEntity<Badge> mapBadgeToUser(@PathVariable Long badgeId, @PathVariable Long userId){
//        return status(HttpStatus.OK).body(badgeService.mapBadgeToUser(badgeId, userId));
//    }



}
