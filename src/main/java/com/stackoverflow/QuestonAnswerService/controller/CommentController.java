package com.stackoverflow.QuestonAnswerService.controller;

import com.stackoverflow.QuestonAnswerService.model.Comment;
import com.stackoverflow.QuestonAnswerService.service.CommentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.ResponseEntity.status;

@RestController
@AllArgsConstructor
@RequestMapping("/stackoverflow/comments")
public class CommentController {

    private final CommentService commentService;

    @PostMapping("")
    public ResponseEntity<?> saveComment(@RequestBody Comment comment){
        commentService.saveComment(comment);
        return  new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/{commentId}")
    public ResponseEntity<Comment> getCommentById(@PathVariable Long commentId){
        return status(HttpStatus.OK).body(commentService.getCommentById(commentId));
    }

    @PutMapping("/answer/{answerId}/{commentId}")
    public ResponseEntity<Comment> mapCommentToAnswer(@PathVariable Long answerId, @PathVariable Long commentId){
        return status(HttpStatus.OK).body(commentService.mapCommentToAnswer(answerId, commentId));
    }

    @PutMapping("/user/{userId}/{commentId}")
    public ResponseEntity<Comment> mapCommentToUser(@PathVariable Long userId, @PathVariable Long commentId){
        return status(HttpStatus.OK).body(commentService.mapCommentToUser(userId, commentId));
    }

}
