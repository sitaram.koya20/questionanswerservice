package com.stackoverflow.QuestonAnswerService.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnswerResponse {

    private Long answerId;
    private String answerText;
    private Long questionId;
    private String userName;
    private Integer voteCount;
//    private Integer commentCount;
}
