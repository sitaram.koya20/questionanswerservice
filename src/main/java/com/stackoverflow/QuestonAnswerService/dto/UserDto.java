package com.stackoverflow.QuestonAnswerService.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private Long userId;
    private String userName;
    private String email;
    private Integer tagCount;
    private String badgeGiven;
}
