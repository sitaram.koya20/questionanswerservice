package com.stackoverflow.QuestonAnswerService.dto;

import com.stackoverflow.QuestonAnswerService.model.VoteType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VoteDto {
    private VoteType voteType;
    private Long answerId;
}
