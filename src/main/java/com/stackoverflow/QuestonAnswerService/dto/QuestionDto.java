package com.stackoverflow.QuestonAnswerService.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionDto {
    private Long questionId;
    private String questionTitle;
    private String questionDescription;
    private Integer tagCount;
    private String userName;
    private Integer answerCount;
}
