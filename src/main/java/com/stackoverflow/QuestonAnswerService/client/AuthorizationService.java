package com.stackoverflow.QuestonAnswerService.client;


import com.stackoverflow.QuestonAnswerService.model.RequestType;

import javax.servlet.http.HttpServletRequest;

public interface AuthorizationService {
    RequestType authorizeRequest(RequestType requestType) throws Exception;


    String getUserName(HttpServletRequest request);
}
