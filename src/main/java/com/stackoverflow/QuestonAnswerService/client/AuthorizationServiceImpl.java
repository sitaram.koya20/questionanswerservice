package com.stackoverflow.QuestonAnswerService.client;

import com.stackoverflow.QuestonAnswerService.model.RequestType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

@Service
public class AuthorizationServiceImpl implements  AuthorizationService{

    @Autowired
    RestTemplate restTemplate;

    private final String  loginServiceURL = "http://localhost:7000/auth/authorizeRequest";

    @Override
    public RequestType authorizeRequest(RequestType requestType) throws Exception {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<RequestType> entity = new HttpEntity<>(requestType, headers);
        RequestType processedRequest = restTemplate.exchange(loginServiceURL, HttpMethod.POST,
                entity,
                RequestType.class).getBody();

        if(!processedRequest.isAuthorized())
            throw new Exception("User is not authorized to perform this request");
            return processedRequest;

    }

    @Override
    public String getUserName(HttpServletRequest request) {
        return (String) request.getAttribute("name");
    }
}
